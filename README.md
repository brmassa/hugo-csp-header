# Hugo CSP Header

This module implements Content Security Policy (CSP) into your [Hugo](https://gohugo.io/) site. It will create a `_header` address for your site.

You can expand it using defined configurations in your config file.

## Install

Inside your site's main config

YAML

```yaml
module:
  imports:
    - path: gitlab.com/brmassa/hugo-csp-header
```

TOML

```toml
[module]
[[module.imports]]
path = "gitlab.com/brmassa/hugo-csp-header"
```

It will be automatically installed as a Hugo module when you build your site or serve it locally.

### Update

To update *only this* module, use the terminal command:

```terminal
hugo mod get -u gitlab.com/brmassa/hugo-csp-header
```

To update *all* modules, use the terminal command:

```terminal
hugo mod get -u
```

## Setup

in the same file as before, in the `Params` section, add:

YAML

```yaml
params:
  contentSecurityPolicy:
    base-uri:
      - "'self'"
    connect-src:
      - "'self'"
    default-src:
      - "'none'"
    font-src:
      - "'self'"
    form-action:
      - "'self'"
    frame-ancestors:
      - "'self'"
    frame-src:
      - "'self'"
    img-src:
      - "*"
      - "'self'"
      - "data:"
      - "content:"
    manifest-src:
      - "'self'"
    media-src:
      - "'none'"
    script-src:
      - "'self'"
    style-src:
      - "'self'"
```

TOML

```toml
[params.contentSecurityPolicy]
  base-uri = ""
  connect-src = ""
  default-src = ""
  font-src = ""
  form-action = ""
  frame-ancestors = ""
  frame-src = ""
  img-src = ""
  manifest-src = ""
  media-src = ""
  script-src = ""
  style-src = ""
```
